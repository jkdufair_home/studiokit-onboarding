﻿namespace StudioKit.Onboarding
{
	public static class OnboardingConstants
	{
		// https://www.purdue.edu/apps/account/html/chars.txt
		public const string AdminProfessionalStaff = "13101";

		public const string Faculty = "13105";
		public const string LimitedTermLecturer = "13111";
		public const string VisitingOrEmeritusFaculty = "13124";
		public const string ManagementProfessional = "13112";
		public const string ClinicalResearch = "13103";
		public const string ContinuingLecturer = "13104";
		public const string GraduateTeachingAssistant = "10362";
	}
}