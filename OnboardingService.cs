﻿using System.Linq;
using System.Threading.Tasks;

namespace StudioKit.Onboarding
{
	public abstract class OnboardingService<TUser> : IOnboardingService<TUser>
	{
		public bool IsShibbolethInstructor(string[] employeeType)
		{
			return employeeType.Contains(OnboardingConstants.AdminProfessionalStaff)
					|| employeeType.Contains(OnboardingConstants.ManagementProfessional)
					|| employeeType.Contains(OnboardingConstants.Faculty)
					|| employeeType.Contains(OnboardingConstants.LimitedTermLecturer)
					|| employeeType.Contains(OnboardingConstants.VisitingOrEmeritusFaculty)
					|| employeeType.Contains(OnboardingConstants.ClinicalResearch)
					|| employeeType.Contains(OnboardingConstants.ContinuingLecturer)
					|| employeeType.Contains(OnboardingConstants.GraduateTeachingAssistant);
		}

		public void SetUpInstructor(TUser user, string[] employeeType)
		{
			if (!IsShibbolethInstructor(employeeType))
				return;
			if (!IsUserInstructor(user))
				AddUserToInstructorRole(user);
			CreateInstructorSandbox(user);
		}

		public async Task SetUpInstructorAsync(TUser user, string[] employeeType)
		{
			if (!IsShibbolethInstructor(employeeType))
				return;
			if (!await IsUserInstructorAsync(user))
				await AddUserToInstructorRoleAsync(user);
			await CreateInstructorSandboxAsync(user);
		}

		protected abstract Task<bool> IsUserInstructorAsync(TUser user);

		protected abstract bool IsUserInstructor(TUser user);

		protected abstract Task AddUserToInstructorRoleAsync(TUser user);

		protected abstract void AddUserToInstructorRole(TUser user);

		protected abstract Task CreateInstructorSandboxAsync(TUser user);

		protected abstract void CreateInstructorSandbox(TUser user);
	}
}