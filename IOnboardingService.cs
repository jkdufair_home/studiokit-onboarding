﻿using System.Threading.Tasks;

namespace StudioKit.Onboarding
{
	public interface IOnboardingService<TUser>
	{
		bool IsShibbolethInstructor(string[] employeeType);

		void SetUpInstructor(TUser user, string[] employeeType);

		Task SetUpInstructorAsync(TUser user, string[] employeeType);
	}
}